# DoubleHead

## Thanks

Initial repository with server and middleware to use: https://gitlab.com/ka-ba/doublehead.git

## Server

Run the server on an accessible maschine with -- the jar file in the releases folder was compiled with java 17 and hence will not run with java 8.

```console
java -jar doublehead-server.jar server.fun.de 1709
```

To test it on your developer maschine check your ip and use it instead of the host name -- do not use localhost, as the emulator will not be able to communicate with localhost.

To be able to access your server with your app, i.e. from any other maschine/mobile:

- configure your router to let it through (Fritzbox: Internet - Freigaben)
- configure/switch off your firewall

## Middleware

Just copy the latest version of doublehead-middleware.jar (folder dist) of the above mentioned repository DoubleHeadDroid\app\libs\DoubleHeadMiddleware.jar.

## Frontend

### Swing

For testing start several players with

```console
java -jar doublehead-swing.jar test1
```

The card images are missing.

### Android

If you run the server locally, use it's ip in the connection dialog. Known issues

- If the connection fails, the app continues to wait.
- In case of an error, the server needs to be restarted and all clients too.

## Licence

GNU General Public License v3.0
