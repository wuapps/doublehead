package de.wuapps.doublehead.frontend.droid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import java.util.Objects;

import me.kaba.doublehead.middleware.VersionInfo;

public class ConnectFragment extends Fragment {

    private String port, host, userName;
    private EditText txtUserName, txtHost, txtPort;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_connect, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadPreferences();
        txtHost = view.findViewById(R.id.txtHost);
        txtHost.setText(host);
        txtPort = view.findViewById(R.id.txtPort);
        txtPort.setText(port);
        txtUserName = view.findViewById(R.id.txtUserName);
        txtUserName.setText(userName);

        TextView versionView = view.findViewById(R.id.lblVersion);
        versionView.setText(String.format(getString(R.string.version_info), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, VersionInfo.getInstance().toString()));
        view.findViewById(R.id.btnConnect).setOnClickListener(view1 -> {
            savePreferences();
            ConnectFragmentDirections.ActionConnectFragmentToGameFragment action = ConnectFragmentDirections.actionConnectFragmentToGameFragment();
            action.setHost(host);
            action.setPort(port);
            action.setUsername(userName);
            Navigation.findNavController(view1).navigate(action);
        });
        WebView wv = view.findViewById(R.id.wvIntro);
        wv.loadDataWithBaseURL(null, getString(R.string.intro), "text/html", "utf-8", null);
    }

    private void loadPreferences() {
        SharedPreferences sharedPref = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.connect), Context.MODE_PRIVATE);
        port = sharedPref.getString(getString(R.string.preferences_port), "1709");
        userName = sharedPref.getString(getString(R.string.preferences_username), "x");
        host = sharedPref.getString(getString(R.string.preferences_host), "localhost");
    }

    private void savePreferences() {
        host = txtHost.getText().toString().trim();
        port = txtPort.getText().toString().trim();
        userName = txtUserName.getText().toString().trim();
        SharedPreferences sharedPref = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.connect), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.preferences_host), host);
        editor.putString(getString(R.string.preferences_port), port);
        editor.putString(getString(R.string.preferences_username), userName);
        editor.apply();
    }
}