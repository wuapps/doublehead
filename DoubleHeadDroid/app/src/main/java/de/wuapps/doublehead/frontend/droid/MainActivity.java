package de.wuapps.doublehead.frontend.droid;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MainActivity extends AppCompatActivity {

    private TextView txtHelp;
    private static Map<String, Drawable> cardImages = null;
    final ExecutorService asyncExecuter = Executors.newSingleThreadExecutor();
    final Handler asyncHandler = new Handler(Looper.getMainLooper());
    ProgressBar progressBar;
    private static Drawable defaultCardDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtHelp = findViewById(R.id.txtHelp);
        txtHelp.setVisibility(View.GONE);
        progressBar = findViewById(R.id.progressBarCyclic);
        defaultCardDrawable =  ContextCompat.getDrawable(this, R.drawable.back);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadCardImages(this.getApplicationContext());
    }

    private void loadCardImages(Context context){
        asyncExecuter.execute(() -> {
            //Background work here
            Log.d("race", "start loading");
            HashMap<String, Drawable> mapImages = new HashMap<>();
            mapImages.put("k9", ContextCompat.getDrawable(context, R.drawable.ka9));
            mapImages.put("kZ", ContextCompat.getDrawable(context, R.drawable.ka10));
            mapImages.put("kB", ContextCompat.getDrawable(context, R.drawable.kab));
            mapImages.put("kD", ContextCompat.getDrawable(context, R.drawable.kad));
            mapImages.put("kK", ContextCompat.getDrawable(context, R.drawable.kak));
            mapImages.put("kA", ContextCompat.getDrawable(context, R.drawable.kaa));
            mapImages.put("K9", ContextCompat.getDrawable(context, R.drawable.k9));
            mapImages.put("KZ", ContextCompat.getDrawable(context, R.drawable.k10));
            mapImages.put("KB", ContextCompat.getDrawable(context, R.drawable.kb));
            mapImages.put("KD", ContextCompat.getDrawable(context, R.drawable.kd));
            mapImages.put("KK", ContextCompat.getDrawable(context, R.drawable.kk));
            mapImages.put("KA", ContextCompat.getDrawable(context, R.drawable.ka));
            mapImages.put("H9", ContextCompat.getDrawable(context, R.drawable.h9));
            mapImages.put("HZ", ContextCompat.getDrawable(context, R.drawable.h10));
            mapImages.put("HB", ContextCompat.getDrawable(context, R.drawable.hb));
            mapImages.put("HD", ContextCompat.getDrawable(context, R.drawable.hd));
            mapImages.put("HK", ContextCompat.getDrawable(context, R.drawable.hk));
            mapImages.put("HA", ContextCompat.getDrawable(context, R.drawable.ha));
            mapImages.put("P9", ContextCompat.getDrawable(context, R.drawable.p9));
            mapImages.put("PZ", ContextCompat.getDrawable(context, R.drawable.p10));
            mapImages.put("PB", ContextCompat.getDrawable(context, R.drawable.pb));
            mapImages.put("PD", ContextCompat.getDrawable(context, R.drawable.pd));
            mapImages.put("PK", ContextCompat.getDrawable(context, R.drawable.pk));
            mapImages.put("PA", ContextCompat.getDrawable(context, R.drawable.pa));
            mapImages.put("back", defaultCardDrawable);
            asyncHandler.post(() -> {
                Log.d("race", "end loading");
                //UI Thread work here
                cardImages = new HashMap<String, Drawable>(mapImages);
                progressBar.setVisibility(View.GONE);
            });
        });

    }

    public static Drawable getIconToCardHandle(String handle){
        if (cardImages == null)
            return defaultCardDrawable; //should never happen ...
        return cardImages.getOrDefault(handle, defaultCardDrawable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_ping) {
            if (GameController.getInstance().ping())
                Toast.makeText(this, "pinged", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "no session", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.action_help) {
            if (txtHelp.getVisibility() == View.VISIBLE)
                txtHelp.setVisibility(View.GONE);
            else
                txtHelp.setVisibility(View.VISIBLE);
        }
        if (id == R.id.action_credits) {
            Toast.makeText(getApplicationContext(), getString(R.string.credits), Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }


}