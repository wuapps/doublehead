package de.wuapps.doublehead.frontend.droid;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.kaba.doublehead.middleware.Cards;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder>  {
    final private Context mCtx;
    final private List<Cards.Card> cards;
    private int selectedCardPosition = -1;
    private int cardPlayedPosition = -1;
    public CardAdapter(Context mCtx, List<Cards.Card> cards) {
        this.mCtx = mCtx;
        this.cards = cards;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.viewholder_card, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        Cards.Card card = cards.get(position);
            if (position == selectedCardPosition)
                holder.imageViewCard.setBackgroundColor(mCtx.getColor(R.color.cardSelected));
            else
                holder.imageViewCard.setBackgroundColor(Color.TRANSPARENT);
            holder.imageViewCard.setContentDescription(card.toolTip);
            holder.imageViewCard.setImageDrawable(MainActivity.getIconToCardHandle(card.handle));

    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public void removeCard(){
        if (cardPlayedPosition != -1)
            notifyItemRemoved(cardPlayedPosition);
        else
            notifyDataSetChanged();
        cardPlayedPosition = -1;
    }

    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final ImageView imageViewCard;

        public CardViewHolder(View itemView) {
            super(itemView);
            imageViewCard = itemView.findViewById(R.id.ivCard);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int index = getLayoutPosition();
            if (index < 0 || index >= cards.size()) {
                return; //something went wrong ...
            }
            if (selectedCardPosition == index) {
                Cards.Card card = cards.get(index);
                    if (GameController.getInstance().playCard(card)) {
                        cardPlayedPosition = index;
                        selectedCardPosition = -1;
                    }
            } else {
                notifyItemChanged(selectedCardPosition);
                selectedCardPosition = index;
                notifyItemChanged(selectedCardPosition);
            }
        }

    }


}

