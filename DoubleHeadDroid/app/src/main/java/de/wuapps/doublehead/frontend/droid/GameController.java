package de.wuapps.doublehead.frontend.droid;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

import me.kaba.doublehead.middleware.Cards;
import me.kaba.doublehead.middleware.PlayerMgr;
import me.kaba.doublehead.middleware.Savvy;
import me.kaba.doublehead.middleware.Session;

public class GameController implements Savvy {
    private static GameController instance;
    private Session session;
    private String myName;
    private Context context;
    private GameFragment gameFragment;

    private GameController() {
    }

    public synchronized static GameController getInstance() {
        if (instance == null) {
            instance = new GameController();
        }
        return instance;
    }

    public boolean connect(String host, String userName, String port) {
        Properties properties = new Properties();
        properties.setProperty(PK_USERNAME, userName);
        properties.setProperty(PK_HOST, host);
        properties.setProperty(PK_PORT, port);
        session = new Session(this, properties);
        session.go();
        return true;
    }

    @Override
    public void disconnect(String s) {
        if (session != null)
            new Thread(() -> {
                session.end();
                session = null;
            }).start();
    }

    @Override
    public void envisTrickVoters(boolean visible, boolean enable) {
        if (enable && isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.vote());
    }

    @Override
    public void enablePlayCard(boolean canPlay) {
        if (isGameFragmentSet())
            if (canPlay)
                Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() ->gameFragment.hideWait());
            else
                Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() ->gameFragment.showWait());
    }

    @Override
    public void setupGameView(List<String> orderOptions) {
        if (orderOptions == null || orderOptions.isEmpty())
            return;
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showWait());
    }
    @Override
    public void showCurrent(List<PlayerMgr.Player> players, List<Cards.Card> cards) {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.setCurrent(getNames(players), cards));
    }

    @Override
    public void showMessageDialog(Object o, String s, int i) {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> Toast.makeText(context, s + ": " + o.toString(), Toast.LENGTH_LONG).show());
    }

    @Override
    public void showNewHand(List<Cards.Card> cards) {
        Cards.getInstance().sort(cards, "simpel");
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> {
                gameFragment.setMyCards(cards);
                gameFragment.hideWait();
            });
    }

    @Override
    public void showRecent(List<PlayerMgr.Player> players, List<Cards.Card> cards) {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showLastTrick(cards));
    }

    @Override
    public void showTricks(List<PlayerMgr.Player> players, List<Integer> tricks) {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.setTricks(getNames(players), tricks));
    }

    @Override
    public void jubilate(List<PlayerMgr.Player> players) {
        Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() ->gameFragment.jubilate());
    }

    @Override
    public void startObserving() {
        if (isGameFragmentSet()) {
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showWait());
        }
    }

    @Override
    public void stopObserving() {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showWait());
    }

    @Override
    public void showTricksDialog( List<List<Cards.Card>> tricks) {
        int sumPoints = tricks.stream().collect(Collectors.summingInt(trick -> trick.stream().collect(Collectors.summingInt(card -> card.points))));
        long sumFoxes = tricks.stream().collect(Collectors.summingLong(trick -> trick.stream().filter(card -> card.handle.equals("kA")).collect(Collectors.toList()).stream().collect(Collectors.counting())));
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showGameResult(String.format("Spiel: %d Punkte, davon %d Füchse (gefangen?)", sumPoints, sumFoxes)));
    }

    @Override
    public void showPlayers(List<PlayerMgr.Player> players, int max) {
        if (isGameFragmentSet())
            Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.showPlayers(players));
    }

    @Override
    public void removePlayed(Cards.Card card) {
        Objects.requireNonNull(gameFragment.getActivity()).runOnUiThread(() -> gameFragment.removePlayed(card));
    }

    public boolean playCard(Cards.Card card) {
        Log.d("race", "playcard in viewmodel for " + card.handle);
        boolean ok = true;
        try {
            session.playCard(card);
            Log.d("race", "playcard in viewmodel set enableplay to false ");
            enablePlayCard(false);
        }
        catch(IllegalStateException ex){
            gameFragment.showIllegalCardPlay();
            ok = false;
        }
        return ok;
    }

    public void setGameFragment(GameFragment gameFragment) {
        this.gameFragment = gameFragment;
        context = gameFragment.getContext();
    }

    public void voteTrick(String name) {
        session.voteTrick(name);
    }

    public boolean ping() {
        if (session == null)
            return false;
        session.ping();
        return true;
    }

    private boolean isGameFragmentSet() {
        return (gameFragment != null);
    }

    public boolean isInSession() {
        return (session != null);
    }

    private List<String> getNames(List<PlayerMgr.Player> players){
        List<String> names = new ArrayList<String>();
        players.forEach(player -> names.add(player.name));
        return names;
    }
}
