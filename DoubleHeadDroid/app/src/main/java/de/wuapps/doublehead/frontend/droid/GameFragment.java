package de.wuapps.doublehead.frontend.droid;

import android.animation.Animator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import me.kaba.doublehead.middleware.Cards;
import me.kaba.doublehead.middleware.PlayerMgr;

public class GameFragment extends Fragment {
    private TextView txtPlayers, txtVote, txtLastTrick;
    private RecyclerView recyclerView;
    private ImageButton btnSort;
    private List<Cards.Card> myCards;
    private List<CurrentPlayerView> currentPlayerViews;
    private CardAdapter adapter;
    private LottieAnimationView waitView1, waitView2, waitView3, waitView4, jubilateView;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GameController.getInstance().setGameFragment(this);
        assert getArguments() != null;
        String host = getArguments().getString("host");
        String port = getArguments().getString("port");
        String userName = getArguments().getString("username");
        if (host != null && port != null && userName != null && !GameController.getInstance().isInSession() && !GameController.getInstance().connect(host, userName, port)) {
            Navigation.findNavController(view).navigate(R.id.actionConnectionProblem);
        }

        currentPlayerViews = new ArrayList<>();
        currentPlayerViews.add(new CurrentPlayerView(view.findViewById(R.id.txtNamePlayer1), view.findViewById(R.id.txtTricksPlayer1), view.findViewById(R.id.ivPlayer1)));
        currentPlayerViews.add(new CurrentPlayerView(view.findViewById(R.id.txtNamePlayer2), view.findViewById(R.id.txtTricksPlayer2), view.findViewById(R.id.ivPlayer2)));
        currentPlayerViews.add(new CurrentPlayerView(view.findViewById(R.id.txtNamePlayer3), view.findViewById(R.id.txtTricksPlayer3), view.findViewById(R.id.ivPlayer3)));
        currentPlayerViews.add(new CurrentPlayerView(view.findViewById(R.id.txtNamePlayer4), view.findViewById(R.id.txtTricksPlayer4), view.findViewById(R.id.ivPlayer4)));

        recyclerView = view.findViewById(R.id.rvCards);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getActivity());
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        recyclerView.setLayoutManager(layoutManager);

        txtPlayers = view.findViewById(R.id.txtPlayers);
        btnSort = view.findViewById(R.id.btnSort);
        btnSort.setOnClickListener(v -> showSortMenu());

        jubilateView = view.findViewById(R.id.lottieJubilate);
        jubilateView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                jubilateView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        waitView1 = view.findViewById(R.id.lottieWait1);
        waitView2 = view.findViewById(R.id.lottieWait2);
        waitView3 = view.findViewById(R.id.lottieWait3);
        waitView4 = view.findViewById(R.id.lottieWait4);

        txtLastTrick = view.findViewById(R.id.txtLastTrick);
        txtVote = view.findViewById(R.id.txtVote);
        txtVote.setVisibility(View.GONE);
        if (savedInstanceState != null) {
            myCards = (List<Cards.Card>) savedInstanceState.getSerializable("myCards");
            if (myCards != null) {
                setAdapterForCards();
            }
            String players = savedInstanceState.getString("players");
            if (players != null && !players.isEmpty())
                txtPlayers.setText(players);
            List<CurrentPlayerView> playerViews = (List<CurrentPlayerView>) savedInstanceState.getSerializable("currentTrick");
            if (playerViews != null && playerViews.size() == currentPlayerViews.size()) {
                for (int i = 0; i < 4; i++) {
                    CurrentPlayerView stored = playerViews.get(i);
                    CurrentPlayerView shown = currentPlayerViews.get(i);
                    shown.SetView(stored);
                }
            }
        }
    }

    private void setAdapterForCards() {
        adapter = new CardAdapter(getActivity(), myCards);
        recyclerView.setAdapter(adapter);
    }

    public void removePlayed(Cards.Card card) {
        myCards.remove(card);
        adapter.removeCard();
        btnSort.setVisibility(View.GONE);
        //all attempts with notify, submitList and DiffUtil failed, sometimes the card remained visible
    }

    public void jubilate() {
        jubilateView.setVisibility(View.VISIBLE);
        jubilateView.playAnimation();

    }

    public void showWait() {
        short random = (short) (Calendar.getInstance().getTimeInMillis() % 4);
        switch (random) {
            case 0:
                waitView2.setVisibility(View.VISIBLE);
                waitView2.playAnimation();
                break;
            case 1:
                waitView3.setVisibility(View.VISIBLE);
                waitView3.playAnimation();
                break;
            case 2:
                waitView4.setVisibility(View.VISIBLE);
                waitView4.playAnimation();
                break;
            default:
                waitView1.setVisibility(View.VISIBLE);
                waitView1.playAnimation();
        }
    }

    public void hideWait() {
        waitView1.clearAnimation();
        waitView1.setVisibility(View.GONE);
        waitView2.clearAnimation();
        waitView2.setVisibility(View.GONE);
        waitView3.clearAnimation();
        waitView3.setVisibility(View.GONE);
        waitView4.clearAnimation();
        waitView4.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ArrayList<Cards.Card> serializableCards = (ArrayList<Cards.Card>) myCards;
        outState.putSerializable("myCards", serializableCards);
        outState.putString("players", txtPlayers.getText().toString());
        ArrayList<CurrentPlayerView> serializableTrick = (ArrayList<CurrentPlayerView>) currentPlayerViews;
        outState.putSerializable("currentTrick", serializableTrick);
        super.onSaveInstanceState(outState);
    }

    public void setMyCards(List<Cards.Card> myCards) {
        this.myCards = myCards;
        setAdapterForCards();
        btnSort.setVisibility(View.VISIBLE);
    }

    public void setCurrent(List<String> names, List<Cards.Card> cards) {
        if (names.size() != cards.size())
            Toast.makeText(this.getContext(), "ungleich", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < names.size(); i++) {
            CurrentPlayerView currentPlayerView = currentPlayerViews.get(i);
            currentPlayerView.setName(names.get(i));
            currentPlayerView.setCard(cards.get(i).handle);
        }
    }

    public void setTricks(List<String> names, List<Integer> tricks) {
        disableVoting();
        hideWait();
        if (names.size() != tricks.size())
            Toast.makeText(this.getContext(), "ungleich", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < names.size(); i++) {
            CurrentPlayerView currentPlayerView = currentPlayerViews.get(i);
            if (names.get(i).toLowerCase().trim().equals(currentPlayerView.txtName.getText().toString().toLowerCase().trim()))
                currentPlayerView.setTrick(tricks.get(i));
            else
                Toast.makeText(this.getContext(), "ungleich bei " + names.get(i), Toast.LENGTH_SHORT).show();
        }
    }

    public void showIllegalCardPlay() {
        Toast.makeText(this.getContext(), "momentan ist kein Zug möglich", Toast.LENGTH_SHORT).show();
    }

    public void showPlayers(List<PlayerMgr.Player> players) {
        StringBuilder sb = new StringBuilder();
        for (PlayerMgr.Player player : players) {
            sb.append(player.name);
            sb.append((player.getOnline() ? " (on) " : " (off) "));
        }
        txtPlayers.setText(sb.toString());
    }

    public void vote() {
        hideWait();
        txtVote.setVisibility(View.VISIBLE);
        for (int i = 0; i < currentPlayerViews.size(); i++) {
            CurrentPlayerView currentPlayerView = currentPlayerViews.get(i);
            ImageView iv = currentPlayerView.ivCard;
            String votedPlayer = currentPlayerView.txtName.getText().toString();
            iv.setOnClickListener(view -> {
                view.setBackgroundColor(Objects.requireNonNull(getActivity()).getColor(R.color.cardSelected));
                GameController.getInstance().voteTrick(votedPlayer);
            });
        }
    }

    private void disableVoting() {
        txtVote.setVisibility(View.GONE);
        for (CurrentPlayerView view : currentPlayerViews) {
            view.ivCard.setOnClickListener(null);
            view.ivCard.setBackgroundColor(Objects.requireNonNull(getActivity()).getColor(R.color.colorUnSelected));
        }
    }

    public void showLastTrick(List<Cards.Card> cards) {
        StringBuilder sb = new StringBuilder();
        sb.append("Letzter Stich: ");
        for (Cards.Card card : cards) {
            if (card.handle.contains("back")) return; //kein letzter Stich
            sb.append(card.toolTip);
            sb.append("; ");
        }
        txtLastTrick.setBackgroundColor(Objects.requireNonNull(getContext()).getColor(R.color.primaryLightColor));
        txtLastTrick.setText(sb.toString());
    }

    public void showGameResult(String info) {
        txtLastTrick.setText(info);
    }

    public void sortCards(String sortOption) {
        Cards.getInstance().sort(myCards, sortOption);
        adapter.notifyDataSetChanged();
    }

    public void showSortMenu() {
        PopupMenu popupMenu = new PopupMenu(requireContext(), btnSort);
        Menu menu = popupMenu.getMenu();
        menu.clear();
        List<String> sortOptions = Arrays.asList("simpel", "s-Damen", "s-Buben", "s-Ivan", "s-veggi");
        for (String orderName : sortOptions) {
            MenuItem m = menu.add(orderName);
            m.setOnMenuItemClickListener(item -> {
                sortCards(orderName);
                return true;
            });
        }
        popupMenu.show();
    }

    class CurrentPlayerView implements Serializable {
        final public TextView txtName;
        final public TextView txtTrick;
        final public ImageView ivCard;

        public CurrentPlayerView(TextView txtName, TextView txtTrick, ImageView ivCard) {
            this.txtName = txtName;
            this.txtTrick = txtTrick;
            this.ivCard = ivCard;
        }

        public void SetView(CurrentPlayerView org) {
            txtName.setText(org.txtName.getText());
            txtTrick.setText(org.txtTrick.getText());
            ivCard.setImageDrawable(org.ivCard.getDrawable());
        }

        public void setName(String name) {
            txtName.setText(name);
        }

        public void setTrick(int count) {
            txtTrick.setText(MessageFormat.format(getString(R.string.format_tricks), count));
        }

        public void setCard(String handle) {
            ivCard.setImageDrawable(MainActivity.getIconToCardHandle(handle));
        }
    }
}